## vertretungsplan.io documentation

### 1. API

The API uses HTTP(S) and JSON.

#### 1.1 /vp-content - institution list

Every API server must provide the API ``/vp-content``.
The response should have the following properties:

- institutions (Array)
  - id (String)
  - title (String)
  - contentServerUrl (String, optional)

Example response 1:

```
{
  "institutions": [
    {
      "id": "domgym-mer",
      "title": "Domgymnasium Merseburg"
    },
    {
      "id": "gs-gym-stuttgart",
      "title": "Geschwister-Scholl-Gymnasium Stuttgart"
    }
  ]
}
```

In this example, there is no ``contentServerUrl``.
This informs the client that the content is served from the same server.

Example response 2:

```
{
  "institutions": [
    {
      "id": "cwg-halle",
      "title": "Christian-Wolff-Gymnasium",
      "contentServerUrl": "http://localhost:9000"
    },
    {
      "id": "domgym-mer",
      "title": "Domgymnasium Merseburg",
      "contentServerUrl": "http://localhost:9001"
    }
  ]
}
```

In this example, there is a ``contentServerUrl``.
The content is at the specified server.
The specified server should have the same item in its ``/vp-content`` list.

#### 1.2 /vp-institution/[institution id]/config/[bucket id] - configuration

This API provides the configuration options for the user (if any) and
data which is required to query the content.

For the configuration, there is a key-value-storage.

The response should have the following properties:

- config (Array)
  - type (enum: ``radio``, ``password``, ``otherConfigBucket``, ``text`` or ``divider``)
  - param (String)
    - key in the key-value-storage
    - the password field for ``otherConfigBucket``
  - label (string)
  - value (string)
    - used as value for the key-value-storage for ``radio``
    - used to specify the config bucket id for ``otherConfigBucket``
  - visibilityConditionId (string)
- contentBucketSets (Array)
  - id (string)
  - passwordField (string, optional)
  - usageConditionId (string)
  - type (enum, ``content`` or ``plan``, specifies the API used to query it)
- conditionSets (Array)
  - id (string)
  - type (enum)
    - ``and`` (left, right = condition set id)
    - ``or`` (left, right = condition set id)
    - ``paramIs`` (left = param, right = expectedValue)
    - ``paramNotEmpty`` (left = param, right = empty string)
    - ``not`` (only uses left as condition set id, right = empty string))
  - left (string)
  - right (string)
- configValidationConditionId (string)

Notes:

- there are the internal conditions sets ``_true`` and ``_false``
- condition sets starting with ``_doesClientSupport`` must be evaluated to false if not otherwise specified
- condition sets starting with ``_`` are reserved and evaluating unknown reserved contion sets should fail
- the client should start by querying the config bucket ``default``
- the client can limit the depth of conditionSets
- the client can limit the depth of nesting config buckets
- the client can limit config bucket recursion
- if ``passwordField`` is set, then the linked ressource requires authentication
  - using HTTP Basic Authentication
  - username can be anything
  - password should be the content saved under the key specified in passwordField in the key-value-storage
- a divider does not have a label
- a text shows the value of the ``label`` attribute

Example response:

```
{
  "config": [
    {
      "param": "userType",
      "type": "radio",
      "value": "student",
      "label": "Schüler",
      "visibilityConditionId": "_true"
    },
    {
      "param": "userType",
      "type": "radio",
      "value": "teacher",
      "label": "Lehrer",
      "visibilityConditionId": "_true"
    },
    {
      "param": "studentPassword",
      "type": "password",
      "visibilityConditionId": "isStudent",
      "value": "",
      "label": "Schüler-Passwort"
    },
    {
      "param": "teacherPassword",
      "type": "password",
      "visibilityConditionId": "isTeacher",
      "value": "",
      "label": "Lehrer-Passwort"
    }
  ],
  "configValidationConditionId": "hasValidUserType",
  "contentBucketSets": [
    {
      "id": "student_file",
      "passwordParam": "studentPassword",
      "usageConditionId": "isStudent",
      "type": "content"
    },
    {
      "id": "teacher_file",
      "passwordParam": "teacherPassword",
      "usageConditionId": "isTeacher",
      "type": "content"
    },
    {
      "id": "student_plan",
      "passwordParam": "studentPassword",
      "usageConditionId": "isStudent",
      "type": "plan"
    },
    {
      "id": "teacher_plan",
      "passwordParam": "teacherPassword",
      "usageConditionId": "isTeacher",
      "type": "plan"
    }
  ],
  "conditionSets": [
    {
      "id": "isStudent",
      "type": "paramIs",
      "left": "userType",
      "right": "student"
    },
    {
      "id": "isTeacher",
      "type": "paramIs",
      "left": "userType",
      "right": "teacher"
    },
    {
      "id": "hasValidUserType",
      "type": "or",
      "left": "isStudent",
      "right": "isTeacher"
    }
  ]
}
```

#### 1.3 /vp-institution/[institution id]/content/[bucket id]

This API is used to get the content of content buckets with the type ``content``.
The response should have the following properties:

- file (array)
  - type (``plan`` or ``download``; plan should be more visible in the UI)
  - mimeType (string)
  - title (string)
  - id (string)
  - notify (boolean)
  - file (array)
    - url (string)
    - sha512 (string)
    - size (number, in bytes)
- message (array)
  - id (string)
  - title (string)
  - content (string)
  - notify (boolean)

Notes:

- a file can have multiple items with different urls
  - these belong together (like multiple pages of one document)
  - the client should show them together or provide a submenu to select the item
- the client should use the sorting as provided by the server as base
  - the client should sort the items by type in the following order: message, plan, download

Example response:

```
{
  "file": [
    {
      "type": "plan",
      "mimeType": "text/html",
      "lastModified": 1568379622000,
      "title": "Vertretungsplan Schüler.html",
      "file": [
        {
          "url": "https://example.tld/plan43654.html",
          "sha512": "d04c2b126b356793f9f59e24071791f397f0d379ad8937e33ebf495eb615160ebbf75c2d2facbea5bebb2f4c7c96d7958e3c25c8c1f3a4feb39a747e53805ab5",
          "size": 22688
        }
      ],
      "id": "plan/today",
      "notify": false
    },
    {
      "type": "download",
      "mimeType": "image/png",
      "lastModified": 1559493491000,
      "title": "Hinweise Schülervortrag",
      "file": [
        {
          "url": "https://example.tld/47b5-9b5a_00.png",
          "sha512": "673eb0cd37543bd55ba49ba5f9ed403d7c10b97d482bb9d1f185a1e329c711047aa17dd79e2d973b25a1f8b9347d2b86744373433c8fc6a0b403adcb4da26731",
          "size": 75074
        },
        {
          "url": "https://example.tld/47b5-9b5a_01.png",
          "sha512": "668fc6504cb7ef8fcaa5dc76cb376e01df03d53312c0ee3283eb141f8d1bdb6b8c910de884f855318562c6ff4f5f1a82abbd59962bd9ff058a44dd6a257ac75c",
          "size": 63463
        }
      ],
      "id": "image/png_Hinweise Schülervortrag",
      "notify": false
    }
  ]
  "message": [
    {
      "id": "test",
      "title": "Test-Nachricht",
      "content": "Dies ist ein Test",
      "notify": true
    }
  ]
}
```

#### 1.4 /vp-institution/[institution id]/plan/[bucket id]

This API is used to get the content of content buckets with the type ``plan``.
The response should have the following properties:

- items (Array)
  - date (string, YYYY-MM-DD)
  - class (string)
  - lesson (number)
  - subject (string or null)
  - subjectChanged (boolean)
  - teacher (string or null)
  - teacherChanged (boolean)
  - room (string or null)
  - roomChanged (boolean)
  - info (string or null)

Notes:

- the client should use the sorting as provided by the server as base
  - the client should do a stable sort by the lesson number; this allows merging multiple plan parts

Example response:

```
{
  "items": [
    {
      "date": "2019-09-13",
      "class": "6/3",
      "lesson": 1,
      "subject": "Mat",
      "subjectChanged": false,
      "teacher": "Frau A",
      "teacherChanged": true,
      "room": "3.06",
      "roomChanged": false,
      "info": "für Mat Herr B"
    },
    {
      "date": "2019-09-13",
      "class": "6/4",
      "lesson": 1,
      "subject": "Ges",
      "subjectChanged": true,
      "teacher": "Herr C",
      "teacherChanged": true,
      "room": "2.08",
      "roomChanged": true,
      "info": "für Mat Herr D"
    },
    {
      "date": "2019-09-16",
      "class": "5/5",
      "lesson": 5,
      "subject": "Ges",
      "subjectChanged": true,
      "teacher": "Herr E",
      "teacherChanged": true,
      "room": "1.05",
      "roomChanged": false,
      "info": "für Deu Frau F"
    },
    {
      "date": "2019-09-16",
      "class": "6/4",
      "lesson": 3,
      "subject": "Ges",
      "subjectChanged": true,
      "teacher": "Herr G",
      "teacherChanged": true,
      "room": "1.03",
      "roomChanged": true,
      "info": "für Mat Herr H"
    }
  ]
}
```
